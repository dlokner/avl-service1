# concreate version of base image
FROM python:3

RUN curl -sL https://github.com/openfaas/faas/releases/download/0.9.14/fwatchdog > /usr/bin/fwatchdog \
    && chmod +x /usr/bin/fwatchdog

# run with python3
ENV fprocess="python3 entrypoint.py"
COPY entrypoint.py /

EXPOSE 8080
ENTRYPOINT [ "fwatchdog" ]